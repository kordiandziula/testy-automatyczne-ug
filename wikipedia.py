import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class TestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())

    def tearDown(self):
        self.driver.close()


class Wikipedia(TestCase):
    URL = 'https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna'

    def test_logo(self):
        self.driver.get(self.URL)

        try:
            logo = self.driver.find_element(By.CLASS_NAME, "mw-logo-icon")
        except Exception as e:
            self.fail("Logo not found.")

    def test_create_account(self):
        self.driver.get(self.URL)

        try:
            button = self.driver.find_element(By.ID, "pt-createaccount-2").find_element(By.TAG_NAME, "a")
        except Exception as e:
            self.fail("Create account button not found.")

        button.click()

        try:
            input_username = self.driver.find_element(By.ID, "wpName2")
            input_password = self.driver.find_element(By.ID, "wpPassword2")
            input_confirm_password = self.driver.find_element(By.ID, "wpRetype")
            input_email = self.driver.find_element(By.ID, "wpEmail")
            button_create_account = self.driver.find_element(By.ID, "wpCreateaccount")
        except Exception as e:
            self.fail("Inputs not found.")

        input_username.send_keys("user")
        input_password.send_keys("pass")
        input_confirm_password.send_keys("pass")
        input_email.send_keys("email@email.com")

        url_before = self.driver.current_url
        button_create_account.click()
        url_after = self.driver.current_url

        assert url_after == url_before





